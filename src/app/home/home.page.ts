import { Component, OnInit } from '@angular/core';

//import { User } from 'firebase';
import { User } from '../Modules/user';
import { RegisterPage } from '../register/register.page';
import { AppRoutingModule } from '../app-routing.module';
import { Router } from '@angular/router';
import { importExpr } from '@angular/compiler/src/output/output_ast';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { FormsModule, FormBuilder, FormControl, Validators, AbstractControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { AlertController } from '@ionic/angular';

import * as firebase from 'firebase';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  ngOnInit(): void {
    this.limpiarForm();
  }

  user = {} as User;

  formgroup: FormGroup;
  email: AbstractControl;
  password: AbstractControl;
  mostrar=true;
  private error:string = "sin error";

  
  constructor(private router: Router, private afAuth: AngularFireAuth, formbuilder: FormBuilder, public alertController: AlertController) {
    
    this.limpiarForm();

    this.formgroup = new FormGroup({
      'email': new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.email
      ]),

      'password': new FormControl('', [
        Validators.required,
        Validators.minLength(6)]       
      ),
    })
    this.email = this.formgroup.controls['email'];
    this.password = this.formgroup.controls['password'];

    this.mostrar = false;
  }

  
  async ingresar(usuario: User) {

    console.log("En ingresar");

    if (this.user.correo == "") {
     
      this.email.markAsTouched();
    }
    else if (this.user.clave == "")
    {    
      this.password.markAsTouched();
    }
    else {
      this.traerUsuario(usuario.correo)

      firebase.auth().signInWithEmailAndPassword(usuario.correo, usuario.clave)
        .then((firebaseUser) => {
          if (firebaseUser) {
            this.paginaPrincipal();
          }

        }).catch((error) => {

          this.informarError(error)

          if (error.message.length > 0) {
            this.informarError(error)
          }
        });
    }

  }

  informarError(error: any): any {
    this.router.navigate(['error-logueo']);
  }

  paginaPrincipal(): any {
    this.router.navigate(['menu']);
  }

  registrarse() {
    this.router.navigate(['register']);
  }

  
  limpiarForm():any{
   
    this.user.user = "";
    this.user.correo = "";
    this.user.clave = "";
    return "";
  }

  traerUsuario(mailActual: string):string {
    var usuariosRef = firebase.database().ref().child("usuarios");
    usuariosRef.on("value", (snap) => {
      var data = snap.val();

      for (var key in data) {
        if (mailActual === (data[key]).email) {
          localStorage.setItem("usuarioActual", (data[key].user));
        }
      }
    });
    return "";
  }

  realizarPruebas(){
    this.router.navigate(['test-ingreso']);
  }

  
}
