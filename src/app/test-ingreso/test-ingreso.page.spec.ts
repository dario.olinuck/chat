import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestIngresoPage } from './test-ingreso.page';

describe('TestIngresoPage', () => {
  let component: TestIngresoPage;
  let fixture: ComponentFixture<TestIngresoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestIngresoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestIngresoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
