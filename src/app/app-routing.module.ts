import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { path: 'opciones', loadChildren: './opciones/opciones.module#OpcionesPageModule' },
  { path: 'listado', loadChildren: './listado/listado.module#ListadoPageModule' },
  { path: 'conversacion', loadChildren: './conversacion/conversacion.module#ConversacionPageModule' },
  { path: 'test-ingreso', loadChildren: './test-ingreso/test-ingreso.module#TestIngresoPageModule' },
  { path: 'menu', loadChildren: './menu/menu.module#MenuPageModule' },
  { path: 'opciones-b', loadChildren: './opciones-b/opciones-b.module#OpcionesBPageModule' },
  { path: 'error-logueo', loadChildren: './error-logueo/error-logueo.module#ErrorLogueoPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
