import { Component, OnInit } from '@angular/core';
import { Mensaje } from '../Modules/mensaje';

import * as firebase from 'firebase';


@Component({
  selector: 'app-conversacion',
  templateUrl: './conversacion.page.html',
  styleUrls: ['./conversacion.page.scss'],
})
export class ConversacionPage implements OnInit {

  mensaje: string = "";
  mensajes = new Array<Mensaje>();
  public mostrar:boolean = true;
  nombreUsuario: string= localStorage.getItem('usuarioActual');
  destinatarioMensaje: string = localStorage.getItem('destinatario');

  constructor() {
    this.getMessages();
  }

  ngOnInit() {
  }

  sendMessage() {
 
    
    var messagesRef = firebase.database().ref().child("mensajes");
    messagesRef.push({ mensaje: this.mensaje, nombre: this.nombreUsuario,destinatario:this.destinatarioMensaje });  
    this.mensaje = "";
  }

  getMessages() {

    var messagesRef = firebase.database().ref().child("mensajes");
    messagesRef.on("value", (snap) => {
      var data = snap.val();
      this.mensajes = [];
      for (var key in data) {

        if ((data[key].nombre == this.nombreUsuario && data[key].destinatario == this.destinatarioMensaje)
          || (data[key].destinatario == this.nombreUsuario && data[key].nombre == this.destinatarioMensaje)) {
          this.mensajes.push(data[key]);
        }

      }
      this.mostrar = false;
    });
  }

}
