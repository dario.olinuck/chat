import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { timer } from 'rxjs'


import { FIREBASE_CONFIG } from '../app/app.firebase.config';

import * as firebase from 'firebase';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  // rootPage:any= 'LoginPage';
  showSplash: boolean = true;
  caminoInicial: string = '../../assets/highway.wav';
  acdc: any;


  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      timer(3800).subscribe(() => this.showSplash = false);
    });

    this.iniciarlizarAudios();
    firebase.initializeApp(FIREBASE_CONFIG);
  }

  iniciarlizarAudios() {
    this.acdc = new Audio();
    this.acdc.src = this.caminoInicial;
    this.acdc.load();
    this.acdc.play();       
  }

}
