import { Component, OnInit } from '@angular/core';
import { User } from '../Modules/user';
import { Router } from '@angular/router'
import * as firebase from 'firebase';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.page.html',
  styleUrls: ['./listado.page.scss'],
})
export class ListadoPage implements OnInit {


  usuarios = new Array<User>();
  usuarioActual = localStorage.getItem("usuarioActual");

  constructor(private router: Router) { }

  ngOnInit() {

    this.traerUsuarios();
  }

  traerUsuarios() {
    var usuariosRef = firebase.database().ref().child("usuarios");
    usuariosRef.on("value", (snap) => {
      var data = snap.val();

      for (var key in data) {
          
          if (this.usuarioActual != data[key].user){
            this.usuarios.push(data[key]);  
          }
          

          
        }
      
    });
  }

  ingresarConversacion(usu:User){

    localStorage.setItem('destinatario',usu.user)

    this.router.navigate(['conversacion']);
  }

  salir(){
    this.router.navigate(['login'])
  }
}
